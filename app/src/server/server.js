const express = require('express');
const morgan = require('morgan');
const routes = require('./routes');
const { log, expressMorganStreamLog } = require('../logger');

const app = express();

function createServer(serverPort = 8080) {
  return new Promise((resolve) => {
    function onServerUp() {
      log(`server up and running http://localhost:${serverPort}`);
      resolve();
    }
    app
      .use(morgan('tiny', {
        stream: expressMorganStreamLog,
      }))
      .use(routes)
      .listen(serverPort, onServerUp);
  });
}

module.exports = createServer;
