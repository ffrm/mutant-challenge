const { Router } = require('express');
const indexController = require('./controllers/index');
const logController = require('./controllers/log');
const notfoundController = require('./controllers/notfound');

const router = Router();

router
  .get('/', indexController)
  .get('/logs', logController)
  .get('*', notfoundController);

module.exports = router;
