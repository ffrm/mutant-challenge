function notfoundController(req, res) {
  res.status(404).end();
}

module.exports = notfoundController;
