const { find: findAllUsers } = require('../../repository/user');
const toUsersWebsitesList = require('../../resolvers/user/toUsersWebsitesList');
const toUsersAlphabeticOrderList = require('../../resolvers/user/toUsersAlphabeticOrderList');
const toUsersWithSuiteList = require('../../resolvers/user/toUsersWithSuiteList');

async function indexController(req, res) {
  try {
    const users = await findAllUsers();
    const usersWebsites = toUsersWebsitesList(users);
    const usersInAlphabeticOrder = toUsersAlphabeticOrderList(users);
    const usersWithSuite = toUsersWithSuiteList(users);
    const response = `${usersWebsites}\n${usersInAlphabeticOrder}\n${usersWithSuite}`;
    res.end(response);
  } catch (error) {
    res.status(500).end(error.message);
  }
}

module.exports = indexController;
