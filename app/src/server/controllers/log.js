const { find: findAllLogs } = require('../../repository/log');

async function logController(req, res) {
  try {
    const logSize = 30;
    const logOrder = ['@timestamp:desc'];
    const logs = await findAllLogs({ size: logSize, sort: logOrder });
    res.status(200).end(logs);
  } catch (error) {
    console.log(error);
    res.status(500).end(error.message);
  }
}

module.exports = logController;
