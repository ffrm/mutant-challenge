function getEnv(key, _default) {
  return process.env[key] || _default;
}

module.exports = getEnv;
