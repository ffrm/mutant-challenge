module.exports = function arrayToString(array, separator = '') {
  return array.join(separator);
};
