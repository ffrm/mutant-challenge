const arrayOfObjectsToString = require('./arrayOfObjectsToString');

const input = [
  { name: 'a' },
  {},
  { name: 'c' },
];

describe('Array of objects to string', () => {
  it('should return an empty string when props is not seted', () => {
    expect(arrayOfObjectsToString(input)).toEqual('');
  });

  it('should concat objects name properties with pipe', () => {
    expect(arrayOfObjectsToString(input, {
      props: ['name'],
      separator: '|',
    })).toEqual('a||c');
  });

  it('should display one name per line', () => {
    expect(arrayOfObjectsToString(input, {
      props: ['name'],
      separator: '\n',
    })).toEqual(`a

c`);
  });
});
