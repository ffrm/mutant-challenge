const arrayToString = require('./arrayToString');

describe('Array to string', () => {
  it('should return "a - b - c" for [a,b,c] array input', () => {
    expect(arrayToString(['a', 'b', 'c'], ' - ')).toEqual('a - b - c');
  });
});
