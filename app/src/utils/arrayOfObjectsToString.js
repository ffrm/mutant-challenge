module.exports = function arrayOfObjectsToString(array, { separator = '', props = [], propSeparator = ' - ' } = {}) {
  return array
    .map((object) => (
      props.map((propName) => object[propName]).join(propSeparator)
    ))
    .join(separator);
};
