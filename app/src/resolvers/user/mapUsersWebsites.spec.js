const mapUsersWebsites = require('./mapUsersWebsites');
const users = require('../../../tests/users.json');
const { value: expectedOutput } = require('../../../tests/expected/mapUsersWebsites.json');

describe('Map users with website', () => {
  it('should throw a TypeError for other types rather than array', () => {
    expect(() => mapUsersWebsites({})).toThrow(TypeError);
  });
  it('should return the list of users containing only website strings', () => {
    expect(mapUsersWebsites(users)).toEqual(expectedOutput);
  });
});
