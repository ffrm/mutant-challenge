const sortUsersInAlphabeticOrder = require('./sortUsersInAlphabeticOrder');
const users = require('../../../tests/users.json');
const { value: expectedOutput } = require('../../../tests/expected/sortUsersInAlphabeticOrder.json');

describe('Users in alphabetic order', () => {
  it('should throw a TypeError for other types rather than array', () => {
    expect(() => sortUsersInAlphabeticOrder({})).toThrow(TypeError);
  });
  it('should return the list of users alphabetically ordered', () => {
    expect(sortUsersInAlphabeticOrder(users)).toEqual(expectedOutput);
  });
});
