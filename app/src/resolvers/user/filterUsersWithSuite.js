module.exports = function filterUsersWithSuite(users = []) {
  return users
    .filter(({ address }) => {
      if (!address || typeof address !== 'object') {
        return false;
      }
      const { suite } = address;
      return /suite/i.test(suite);
    });
};
