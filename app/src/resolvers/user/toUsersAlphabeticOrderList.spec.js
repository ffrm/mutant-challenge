const toUsersAlphabeticOrderList = require('./toUsersAlphabeticOrderList');
const users = require('../../../tests/users.json');
const { value: expectedOutput } = require('../../../tests/expected/toUsersAlphabeticOrderList.json');

describe('Users to users alphabetically ordered list', () => {
  it('should throw a TypeError for other types rather than array', () => {
    expect(() => toUsersAlphabeticOrderList({})).toThrow(TypeError);
  });
  it('should return the list of users alphabetically ordered and  as text', () => {
    expect(toUsersAlphabeticOrderList(users)).toEqual(expectedOutput);
  });
});
