module.exports = function sortUsersInAlphabeticOrder(users = []) {
  return users
    .map(({ name, email, company: { name: companyName } }) => ({
      name,
      email,
      companyName,
    }))
    .sort((objectA, objectB) => {
      if (objectA.name > objectB.name) {
        return 1;
      }
      if (objectB.name > objectA.name) {
        return -1;
      }
      return 0;
    });
};
