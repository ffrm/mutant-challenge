const mapUsersWebsites = require('./mapUsersWebsites');
const arrayToString = require('../../utils/arrayToString');

module.exports = function toUsersWebsitesList(users = []) {
  return arrayToString(mapUsersWebsites(users), '\n');
};
