const filterUsersWithSuite = require('./filterUsersWithSuite');
const users = require('../../../tests/users.json');
const { value: expectedOutput } = require('../../../tests/expected/filterUsersWithSuite.json');

describe('Filter users with suite in address', () => {
  it('should throw a TypeError for other types rather than array', () => {
    expect(() => filterUsersWithSuite({})).toThrow(TypeError);
  });

  it('should return an empty array for list of users without suite address', () => {
    expect(filterUsersWithSuite([{}])).toEqual([]);
  });

  it('should return the list of users with suite in address', () => {
    expect(filterUsersWithSuite(users)).toEqual(expectedOutput);
  });
});
