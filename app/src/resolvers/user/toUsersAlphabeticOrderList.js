const sortUsersInAlphabeticOrder = require('./sortUsersInAlphabeticOrder');
const arrayOfObjectsToString = require('../../utils/arrayOfObjectsToString');

module.exports = function toUsersAlphabeticOrderList(users = []) {
  return arrayOfObjectsToString(
    sortUsersInAlphabeticOrder(users), {
      separator: '\n',
      props: ['name', 'email', 'companyName'],
    },
  );
};
