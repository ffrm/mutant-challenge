const toUsersWithSuiteList = require('./toUsersWithSuiteList');
const users = require('../../../tests/users.json');
const { value: expectedOutput } = require('../../../tests/expected/toUsersWithSuiteList.json');

describe('Users to users with suite list', () => {
  it('should throw a TypeError for other types rather than array', () => {
    expect(() => toUsersWithSuiteList({})).toThrow(TypeError);
  });
  it('should return the list of users names which have "suite" in address', () => {
    expect(toUsersWithSuiteList(users)).toEqual(expectedOutput);
  });
});
