module.exports = function mapUsersWebsites(users = []) {
  return users.map(({ website }) => website);
};
