const filterUsersWithSuite = require('./filterUsersWithSuite');
const arrayOfObjectsToString = require('../../utils/arrayOfObjectsToString');

module.exports = function toUsersWithSuiteList(users = []) {
  return arrayOfObjectsToString(
    filterUsersWithSuite(users), {
      separator: '\n',
      props: ['username'],
    },
  );
};
