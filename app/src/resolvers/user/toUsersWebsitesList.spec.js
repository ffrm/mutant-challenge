const toUsersWebsitesList = require('./toUsersWebsitesList');
const users = require('../../../tests/users.json');
const { value: expectedOutput } = require('../../../tests/expected/toUsersWebsitesList.json');

describe('Users to users websites list', () => {
  it('should throw a TypeError for other types rather than array', () => {
    expect(() => toUsersWebsitesList({})).toThrow(TypeError);
  });
  it('should return the list of users websites as text', () => {
    expect(toUsersWebsitesList(users)).toEqual(expectedOutput);
  });
});
