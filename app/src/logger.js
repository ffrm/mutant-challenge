const winston = require('winston');

const winstonLogger = winston.createLogger({
  format: winston.format.json(),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'app.log', dirname: 'log' }),
  ],
});

function log(message) {
  winstonLogger.log({
    level: 'info',
    message,
  });
}

const expressMorganStreamLog = {
  write(message) {
    log(message);
  },
};

module.exports = {
  log,
  expressMorganStreamLog,
};
