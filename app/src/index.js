const getEnv = require('./utils/env');
const createServer = require('./server/server');

async function main() {
  const serverPort = getEnv('SERVER_PORT');
  await createServer(serverPort);
}

main();
