const got = require('got');
const getEnv = require('../utils/env');

const API_URL = getEnv('API_URL', 'https://jsonplaceholder.typicode.com/users');

async function findAllUsers() {
  const response = await got(API_URL, {
    responseType: 'json',
  });
  const { body } = response;
  return body;
}

module.exports = {
  find: findAllUsers,
};
