const elasticsearch = require('elasticsearch');
const getEnv = require('../utils/env');

const ES_VERSION = getEnv('ES_VERSION');
const ES_HOST_NAME = getEnv('ES_HOST_NAME');
const ES_HOST_PORT = getEnv('ES_HOST_PORT');
const ES_USER = getEnv('ES_USER');
const ES_PASSWORD = getEnv('ES_PASSWORD');
const ES_LOG_INDEX = getEnv('ES_LOG_INDEX');

const ESClientOptions = {
  host: `${ES_HOST_NAME}:${ES_HOST_PORT}`,
  apiVersion: ES_VERSION,
  httpAuth: `${ES_USER}:${ES_PASSWORD}`,
  // log: 'trace',
  log: [{
    type: 'tracer',
    levels: ['error', 'warning'],
  }],
};

const esClient = new elasticsearch.Client(ESClientOptions);

function mapESHitsToLogMessages(hits = []) {
  return hits
    .map(({ _source: { message, '@timestamp': timestamp } }) => (
      `${timestamp} - ${message}`
    ))
    .join('');
}

async function findAllLogs({
  size = 30,
  sort = [],
}) {
  const data = await esClient.search({ index: ES_LOG_INDEX, size, sort });
  const { hits } = data.hits;
  return mapESHitsToLogMessages(hits);
}

module.exports = {
  find: findAllLogs,
};
