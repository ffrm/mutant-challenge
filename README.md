# Mutant dev challenge
Desafio proposto para a vaga de Desenvolvedor Node.js na Mutant.

O desafio foi desenvolvido utilizando:

- Node.js
- Express.js - para o servidor web
- ESLint
- Docker e Docker Compose - para criação de containers
- Jest - para a execução de testes unitários
- Elastic Stack (Elasticsearch, Logstash, Filebeat) - para a coleta de logs da aplicação

## Observações

Abaixo estão anotações à respeito de  algumas coisas que não me ficaram muito claras pela descrição do desafio:

1. Como os dados dos usuários devem ser apresentados, e acabei pensando em duas alternativas:

- A primeira seria apresentar uma única lista de usuários - poderia ser uma resposta em json -, onde a lista deveria passar pelos filtros (itens 1,2,3 da descrição do desafio).

- Enquanto que na segunda resolução, seria apresentar uma única lista, porém esta lista é formada de 3 listas (uma parada cada um dos itens 1,2,3 da descrição do desafio).

Adotei a segunda resolução na hora de apresentar os dados, mas as funções que estão desenvolvidas no código podem ser aproveitadas nas duas resoluções e não estão restritas a apenas uma resolução.

2. Os "logs das interações no elasticsearch": interpretei que as "interações" que deveriam ser salvas no elasticsearch são as mensagens de requisições feitas ao servidor web. Para isto, criei uma rota "/logs" no servidor para exibição das mensagens de logs que estão sendo salvas no elasticsearch.

Espero que o que foi resolvido esteja de acordo com o que foi requisitado, e qualquer alteração necessária me avise para eu alterar, por gentileza.

Gostaria de aproveitar o momento para agradecer novamente pela oportunidade.

## Utilização

**GET** `/` - Retorna a lista de usuários.

**GET** `/logs` - Retorna lista de logs mais recentes cadastrados no Elasticsearch. Este endpoint depende da instância do ES para buscar os logs e portanto pode estar indisponível até que a instância esteja disponível.

## Configuração
Todas as variáveis de ambiente listadas abaixo são opcionais e já estão pré definidas dentro da aplicação, através dos arquivos Dockerfile, docker-compose.yml ou in-code. Sendo assim, não são obrigatórias para inicializaçao da aplicação.

- `SERVER_PORT` - Porta onde o servidor do app irá ser acessado. **Nota:** Caso a porta seja diferente da padrão `8080` setada, então as configurações do Docker devem ser alteradas também.

- `API_URL` - Url utilizada para obter a lista de usuários. **Padrão:** `https://jsonplaceholder.typicode.com/users`

- `ES_VERSION` - Versão do Elasticsearch. **Padrão:** 7.6

- `ES_HOST_NAME` - Hostname para conexão com o Elasticsearch

- `ES_HOST_PORT` - Porta para conexão com o Elasticsearch. **Padrão:** 9200

- `ES_USER` - Usuário Elsaticsearch. **Padrão:** elastic

- `ES_PASSWORD` - Senha Elsaticsearch. **Padrão:** mutant

- `ES_LOG_INDEX` - Nome do index onde o logstash irá utilizar para armazenar as mensagens de log no Elasticsearch. **Padrão:** log

## Executando com Vagrant
Para provisionar uma instância no Virtualbox e executar a aplicação utilizando o docker compose dentro da mesma:

```bash
cd mutant-challenge && vagrant up
```

## Executando com Docker Compose
Os arquivos `docker-compose.yml` estão separados entre os projetos do `app` e do `elk` e resolvem as dependências de cada parte do projeto. Para facilitar a execução dos containers pelo Docker Compose, basta executar o seguinte comando:

```bash
cd mutant-challenge && ./scripts/compose-up.sh
```
e para finalizar todos os serviços:

```bash
cd mutant-challenge && ./scripts/compose-down.sh
```

## Test
Para a execução do código diretamente utilizando Node.js, ou para a execução dos testes com o Jest é necessário a instalação dos módulos:

```bash
cd  mutant-challenge/app && npm install
```

Todos os testes unitários das funções que manipulam a coleção de usuários estão disponíveis dentro das respectivas pastas com as extensões `.spec.js`. Os testes foram desenvolvidos utilizando `Jest` e para executá-los:

```bash
cd mutant-challenge/app && npm run test
```

## Lint
Neste projeto foi utilizado o `ESLint` para lint dos arquivos javascript. Para validação dos arquivos, execute:

```bash
cd mutant-challenge/app && npm run lint
```

## Elasticsearch
Usuário padrão é `elastic` e senha `mutant`.

---
Desenvolvido por [Fernando Rama](https://bitbucket.org/ffrm). Obrigado pela oportunidade!