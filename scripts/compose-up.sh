#!/bin/bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR="$DIR/.."

# cria o volume utilizado para log pelo elasticsearch
docker volume create --name=app_app-log

# starta os serviços do elastic
cd $ROOT_DIR/elk
docker-compose -f docker-compose.yml up --build -d

# starta o app
cd $ROOT_DIR/app
docker-compose -f docker-compose.yml up --build -d

# redireciona para a pasta raíz do projeto
cd $ROOT_DIR
